# dploy

A python script and configuration standard that allows simple webapps and backends to be setup quickly and easily. NGINX only (for now).

## Configuration Format

An example project configuration (dp) looks like this:

```
{
	"name": "A short and clean name",
	"web_root": "path/to/directory/containing/index/./html",

	"dependencies": [
		"names",
		"of",
		"packages"
	],

	"setup_comands": [
		"things",
		"to",
		"run",
		"first",
		"time",
		"round"
	],

	"run_comands": [
		"things",
		"to",
		"do",
		"when",
		"starting",
		"up"
	]
}
```

Dependencies, setup\_comands and run\_commands are optional if only hosting a webapp or static files.

## Usage

Before first use, run main.py without any arguments. This will create the config file '~/.config/dploy/config' if it does not already exist.
In that file you can set the nginx configuration location and the place where files will be kept. Then, run it like this:

```
python main.py <git_repo> <url>
```

This will set everything up and start the server. Done!
