import os
import sys
import json

# Get user home
home = os.path.expanduser("~")

# Check if config exists, otherwise copy from example
cf = os.path.join(home, ".config/dploy")
if os.path.exists(cf + "/config"):
    print "Found configuration..."
else:
    print "No configuration found, creating new one..."

    os.system("mkdir " + cf)
    with open(cf + "/config", "w") as fp:
        fp.write("NGINX_HOME = '/etc/nginx/sites'\nFILE_LOCATION = '/var/www/'")

# Check if dry run
if len(sys.argv) == 1:
    print "No arguments provided, exiting!"
    sys.exit()

# Read config
with open(cf + "/config", "r") as fp:
    for i in fp.read().split("\n"):
        exec(i)

# Pull location
pl = os.path.join(FILE_LOCATION, sys.argv[2].replace(".", "_") + "/")

# If it doesnt already exist
if not os.path.exists(pl + "dp.json"):

    # Pull repo to location and free up files
    os.system("sudo git clone " + sys.argv[1] + " " + pl)
    os.system("sudo chmod -R o+r " + pl)
    os.system("sudo chmod -R 755 " + FILE_LOCATION)

    # Read dp
    with open(pl + "dp.json") as json_data:
        dp = json.load(json_data)

    # Setup web server
    with open(os.path.join(NGINX_HOME, sys.argv[2]), "w") as fp:
        fp.write("""
        server {
    listen 80;
    listen [::]:80;

    root """ + os.path.join(pl, dp["web_root"]) + """;
    index index.html index.htm;

    server_name """ + sys.argv[2] + """ www.""" + sys.argv[2] + """;

    location / {
        try_files $uri $uri/ =404;
    }
}
        """)

    # Restart nginx
    os.system("sudo service nginx restart")

    if "run_commands" in dp.keys():
        for i in dp["run_commands"]:
            os.system("cd {} & screen -d -m -S {} {}".format(os.path.join(pl, dp["server_root"]), i.replace(" ", "_").replace(".", "_"), i))

print "Done! Use certbot to apply SSL certificates."
